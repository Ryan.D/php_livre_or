<?php ob_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div id="TITRE">
        <h1>Livre d'or</h1>
    </div>

    <div id="FORMULAIRE">
        <form method="POST" action="insert_msg.php">
            <label for="Pseudo">Entrez votre pseudo</label>
            <input type="text" name="Pseudo" id="psPseudoeudo" placeholder="Pseudo"><br>
            <label for="Message"></label>
            <textarea name="Message" id="Message" cols="30" rows="10" placeholder="Ecrivez votre message"></textarea><br>
            <input type="submit" value="Enregistrez">
        </form>
    </div>

    <?php
    //Requete SQL
    require "bddconfig.php";
    $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);

    $listeMessage = $objBdd->prepare("SELECT * FROM message ORDER BY Date DESC");
    $listeMessage->execute(); ?>
    <div id="CLC">
        <div id="test">
            <h2>Message précédent</h2> <br>
        </div>


            <?php foreach ($listeMessage as $msg) { ?>
                <div class="MSGPRE">
                    <h4><?php echo $msg['Pseudo']; ?>:</h4>
                    <p><?php echo $msg['Message']; ?></p>
                    <p><?php echo $msg['Date']; ?></p>
                </div>
            <?php }
            $listeMessage->closeCursor();
            ?>


    </div>






</body>

</html>