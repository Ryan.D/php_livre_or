<?php


require "bddconfig.php";


$paramOK = false;
// Recup les 3 variables POST et les sécurise
if ((isset($_POST['Pseudo'])) && (isset($_POST['Message']))) {
    $Pseudo = htmlspecialchars($_POST['Pseudo']);
    $Message = htmlspecialchars($_POST['Message']);
    $paramOK = true;
}

// INSERT dans la base
if ($paramOK == true) {
    $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
    $objBdd -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $addBassin = $objBdd->prepare("INSERT INTO message (Message, Pseudo) VALUES (:Message,:Pseudo)");
    $addBassin->bindParam(':Pseudo', $Pseudo, PDO::PARAM_STR);
    $addBassin->bindParam(':Message', $Message, PDO::PARAM_STR);
    $addBassin->execute();

    $lastId = $objBdd -> lastInsertId();
    echo $lastId;

    $serveur = $_SERVER['HTTP_HOST'];
    $chemin = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
    $page = 'index.php';

    header("Location: http://$serveur$chemin/$page");
}?>